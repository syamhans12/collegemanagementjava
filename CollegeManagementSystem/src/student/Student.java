//Syahmi Hanis, Aris Alang, Yusof, Musab
package student;
import java.util.Scanner;
import java.util.Vector;
import java.io.File;
import java.lang.NullPointerException;
import report.*;
import staff.*;
import credit.*;
import onlinebanking.*;
import register.*;
import payment.*;

public class Student 

{
	boolean exit;
	String filepath ="student.txt";
	
	
	
	private static Scanner x;
	

	
	
/*-----------------------MAIN MENU-----------------------------------*/
	
	private void printHeader()
	{
		System.out.println("\n\n\n|                                          |");
		System.out.println("|   Welcome to College Management System   |");
		System.out.println("|                                          |");
	}
	
	private void printMenu()
	{
		System.out.println("\n|                                          |");
		System.out.println("|       Please make a selection            |");
		System.out.println("|            1) Student                    |");
		System.out.println("|            2) Staff                      |");
		System.out.println("|            0) Exit                       |");
		System.out.println("|                                          |");
	}
	
	private int getInput()
	{
		Scanner kb = new Scanner (System.in);
		int choice = -1;
		while (choice <0 || choice > 2)
		{
			try {
				System.out.print("\nEnter your selection : ");
				choice=Integer.parseInt(kb.nextLine());
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid selection.Please try again");
			}
		}
		return choice;
	}
	
	
	
	private void performAction(int choice)
	{
		switch (choice)
		{
		case 0:
			 System.out.println("Thank you for using our system");
			 break;
		case 1:
			System.out.println("\n\n|__________________________________________|");
			System.out.println("|            Menu for Student              |\n");
			runStudentMenu();
			
			break;
		case 2:
			Staff t = new Staff();
			t.insertStaffInfo();
			
			
			break;
		default:
			System.out.println("\n\nUnknown error has occured");
				
			
		}
	}
	
	public void runMenu()
	{
		printHeader();
		 do {
			printMenu();
			int choice= getInput();
			performAction(choice);
		} while (exit);
		
	}
	
	/*----------------------------------------------------------------------------------------*/
	
	
	/*--------------------STUDENT MENU--------------------------------------------------------*/
	
	public void runStudentMenu()
	{
		do
		{
			printStudentMenu();
			int choice=getStudentInput();
			performStudentAction(choice);
			
		}while (exit);
	}
	
	private void printStudentMenu()
	{
		System.out.println("|            1) Register Room              |");
		System.out.println("|            2) Report                     |");
		System.out.println("|            3) Payment                    |");
		System.out.println("|            4) Back to main menu          |");
		System.out.println("|                                          |");
	}
	
	private int getStudentInput()
	{
		Scanner kb = new Scanner (System.in);
		int choice = -1;
		while (choice <0 || choice > 4)
		{
			try {
				System.out.print("\nEnter your selection : ");
				choice=Integer.parseInt(kb.nextLine());
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid selection.Please try again");
			}
		}
		return choice;
	}
	
	private void performStudentAction(int choice)
	{
		
		switch (choice)
		{
		case 1:
			 System.out.println("\n\nRegistration Students's Menu");
			 Register r = new Register();
			 r.addRecord();
			 r.storeInFile(); 
			 runStudentMenu();
			
			 
			 break;
		case 2:
			Report R;
		        String reportType;
		        String description;
		        String block;
		        String room;
	            Scanner in = new Scanner(System.in);
	            System.out.println("\n\nReport");
	            System.out.println("\n1. Missing Key");
	            System.out.println("2. Key Damaged");
	            System.out.println("3. Block Problems");
	            System.out.println("4. Others");
	            System.out.printf("\n\nEnter your choice: ");
	            
	            int report_choice = in.nextInt(); 
	            switch(report_choice) 
	            {
		            case 1:
		                reportType = "Missing Key";
		                System.out.printf("\nBlock: ");
		                block = in.next();
		                System.out.printf("\nRoom no: ");
		                room = in.next();
		                R = new Report(reportType,block,room);
		                R.saveReport();
		                break;
		            case 2:
		                reportType = "Key Damaged";
		                System.out.printf("\nBlock: ");
		                block = in.next();
		                System.out.printf("\nRoom no: ");
		                room = in.next();
		                R = new Report(reportType,block,room);
		                R.saveReport();
		                break;
		            case 3:
		                reportType = "Block Problems";
		                System.out.printf("\nBlock: ");
		                block = in.next();
		                System.out.printf("\nRoom no: ");
		                room = in.next();
		                R = new Report(reportType,block,room);
		                R.saveReport();
		                break;
		            case 4:
		            	reportType = "Others";
		            	System.out.printf("\nBlock: ");
		                block = in.next();
		                System.out.printf("\nRoom no: ");
		                room = in.next();
		                R = new Report(reportType,block,room);
		                R.saveReport();
		                break;
		            default:
		                break;
	            }
	            
	            System.out.println("\n\nYour report has been recorded. We will contact you soon\n\n");
	            runStudentMenu();
	            break;
			
			
		case 3:
			
            System.out.println("\n\n|              Payment Menu                |");
            System.out.println("|                                          |");
    		System.out.printf("Please enter your matric number :");
    		Scanner s = new Scanner(System.in);
    		String tm = s.next();
    	
    		
            verifyMatric(tm, filepath);
            
            break;
		case 4:
			runMenu();
			break;
		default:
			System.out.println("Unknown error has occured");
				
			
		}
	}
	
/***-------------------------------------------------------------------------------------------------------*/
    public void verifyMatric(String mN,String filepath)
    {
    	Payment p = new Payment();
    	Student s = new Student();
    	boolean found = false;
    	String name ="";String tempmatricNo ="";
    	String block="";String room="";
    	double debt=0.0;String statuspayment="";
    	//long cardNo=0;long noAcc=0;
    	
    	
    	try
    	{
    		x= new Scanner(new File(filepath));
    		x.useDelimiter("[,\n]");
    		
    		while(x.hasNext() && !found)
    		{
    			name=x.next();
    			tempmatricNo = x.next();
    			block=x.next();
    			room=x.next();
    			debt=x.nextDouble();
    			statuspayment=x.next();
    			//cardNo=x.nextLong();
    			//noAcc=x.nextLong();
    			
    			if(tempmatricNo.equals(mN))
    			{
    				found = true;
    				
    			}
    		}
    		
    		if(found)
    		{
    			System.out.println("\nThe matric number matched from our system.\n"
						+ "You will be directed to method of payment\n");
				
				p.setPamount(debt);
				p.setVerifyMatric(tempmatricNo);
				System.out.println("Your matric number is "+p.getVerifyMatric());
				System.out.println("Your total debt is RM"+p.getPamount());
				
			
				p.paymentmenu();
    		}
    		else
    		{
    			System.out.println("No record found! Please try again later\n");
				s.runStudentMenu();
    		}
    	}
    	catch(Exception e)
    	{
    		System.out.println("error");
    	}
    }
 
/***************************************************************************************/    	
	public static void main(String[] args) 
	{
		Student menu = new Student();
		menu.runMenu();	
	}

}
