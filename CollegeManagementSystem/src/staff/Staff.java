package staff;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import report.*;
import register.*;
import student.*;

public class Staff {
	
	Scanner sc = new Scanner (System.in);
	private Scanner x;
	Student s = new Student();
	
	private String staffID;
	private String staffPass;
	
	public void setstaffID(String sid){staffID=sid;}
	public void setstaffPass(String pas) {staffPass=pas;}
	
	public String getstaffID() {return staffID;}
	public String getstaffPass() {return staffPass;}
	
	
	
	
/*--------------------STAFF MENU--------------------------------------------------------*/
	boolean exit;
	public void runStaffMenu()
	{
		do
		{
			printStaffMenu();
			int choice=getStaffInput();
			performStaffAction(choice);
			
		}while (exit);
	}
	
	private void printStaffMenu()
	{
		System.out.println("|       1) View Student's Profile          |");
		System.out.println("|       2) View Student's Payment          |");
		System.out.println("|       3) View Student's Report           |");
		System.out.println("|       4) Back to main menu               |");
		System.out.println("|                                          |");
	}
	
	private int getStaffInput()
	{
		Scanner kb = new Scanner (System.in);
		int choice = -1;
		while (choice <0 || choice > 4)
		{
			try {
				System.out.print("\nEnter your selection : ");
				choice=Integer.parseInt(kb.nextLine());
			}
			catch (NumberFormatException e) {
				System.out.println("Invalid selection.Please try again");
			}
			
		}
		return choice;
	}
	
	private void performStaffAction(int choice)
	{
		switch (choice)
		{
		case 1:
			 System.out.println("\n\nList of Student's Profile\n");
			 readStudentProfile();
			 
			 break;
		case 2:
			System.out.println("\n\nList of Payment made by Students");
			readStudentPayment();
			break;
		case 3:
			
			Report r = new Report();
			r.viewReport();
			break;
		case 4:
			Student t = new Student();
			t.runMenu();
			break;
		default:
			System.out.println("Unknown error has occured");
				
			
		}
	}
	
	/*-------------------------------------------------------------------------------------------------------*/
	
	Register reg = new Register();
	public void readStudentProfile()
	{

		String filename = "student.txt";
		File file = new File(filename);
		try {
			Scanner inputStream = new Scanner(file);
			while(inputStream.hasNext())
			{
				String data = inputStream.next();
				String[] values =data.split(",");
				System.out.println("Name: "+ values[0]+"\nMatric Number: "+values[1]
						+"\nBlock: "+values[2]+"\tRoom: "+values[3]+
						
						"\n****************************\n");
			}
			
			inputStream.close();
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		cont();
		
	}

	
	
	/*-------------------------------------------------------------------------------------------------------*/
	
	public void readStudentPayment()
	{
		String filename = "student.txt";
		File file = new File(filename);
		try {
			Scanner inputStream = new Scanner(file);
			while(inputStream.hasNext())
			{
				String data = inputStream.next();
				String[] values =data.split(",");
				System.out.println("Name: "+ values[0]+"\nMatric Number: "+values[1]
						+"\nOutstanding payment: RM"+values[4]
						+"\nPayment method :"+values[5]+
						/*"\nCard Number :"+values[6]+"\nAccount Number :"+values[7]+*/
						"\n****************************\n");
			}
			
			inputStream.close();
			
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		
		cont();
	}
	
	/*-------------------------------------------------------------------------------------------------------*/

	public void cont()
	{
		System.out.println("Press 1 to back to staff menu OR press other number to exit the system");
		
		int option =-1;
		Scanner scn = new Scanner(System.in);
		option=scn.nextInt();
		if(option==1)
		{
			runStaffMenu();
		}else
		{
			System.out.println("Thank you for using our system");
		}
		

		
	}
	
	/*-------------------------------------------------------------------------------------------------------*/
	public void insertStaffInfo()
	{
		String fileD="staff.txt";
		System.out.println("Enter your staff ID :");
		String id = sc.nextLine();
		setstaffID(id);
		System.out.println("Enter your password :");
		String pass = sc.nextLine();
		setstaffPass(pass);
		verifyStaff(getstaffID(),getstaffPass(),fileD);
	}
	
	
	public void verifyStaff(String username,String password,String filedata)
	{
		boolean found = false;
		String user="";
		String passW="";
		
		try 
		{
			x = new Scanner(new File(filedata));
			x.useDelimiter("[,\n]");
			
			while(x.hasNext() && !found)
			{
				user=x.next();
				passW=x.next();
				
				if(user.trim().equals(username.trim())&&passW.trim().equals(password.trim()))
				{
					found= true;
					
					
				}

				
			}
			
			x.close();
			if (found==true)
			{
				System.out.println("The ID and password match!");
				System.out.println("\n\n|__________________________________________|");
				System.out.println("|            Menu for Staff                |\n");
				runStaffMenu();
			}
			else
			{
				System.out.println("The ID and password is not match");
				s.runMenu();
			}
			
		}
		catch(Exception e)
		{
			
				System.out.println("Error");
		}
		
		
		
		
	}
		
	

}
