package payment;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import onlinebanking.*;
import credit.*;
import student.*;
import block.*;




public class Payment
{
	
	private String tempMat;
	public void setVerifyMatric(String tm) {tempMat =tm;}
	
	public String getVerifyMatric() {return tempMat;}
    
    private double Pamount; //outstanding balance set at RM600.00
    private double payfee; // amount of student want to pay
    private double finalAmount; // current debt
    private long noAccount;
    private long cardNo;
    
    private String paymentmethod;
    
    private static Scanner x;
    
    
    Student s = new Student();
    

    Block b = new Block();
    
    
    
    
    public void setPamount(double a) {Pamount=a;}
    public void setPayfee(double pf) {payfee=pf;}
    public void setFinalAmount(double fa) {finalAmount=fa;}
    public void setNoAccount(long p) {noAccount=p;}
    public void setcardNo(long cn) {cardNo=cn;}
   
    public void setPaymentMethod(String pm) {paymentmethod=pm;}
    
    
    
    public double getPamount() {return Pamount;}
    public double getPayfee() {return payfee;}
    public double getFinalAmount() {return finalAmount;}
    public long getNoAccount() {return noAccount;}
    public long getcardNo() {return cardNo;}
    
    public String getPaymentMethod() {return paymentmethod;}
    
    
    public void paymentmenu() {
        
        runPaymentMenu();
        int c=getPaymentInput();
        performPayment(c);
    }

    
    public void runPaymentMenu()
    {
    	System.out.println("\n|                                          |");
        System.out.println("|   Please make a selection of payment     |");
        System.out.println("|            1) Online Banking             |");
        System.out.println("|            2) Debit / Credit             |");
        System.out.println("|            0) Back                       |");
        System.out.println("|                                          |");
    }
    
    public int getPaymentInput()
    {
        Scanner kb = new Scanner (System.in);
        int choice = -1;
        while (choice <0 || choice > 2)
        {
            try {
                System.out.print("\nEnter your selection : ");
                choice=Integer.parseInt(kb.nextLine());
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid selection.Please try again");
            }
            
        }
        return choice;
    }
    
    
    
    public void performPayment(int choice)
    {
        switch (choice)  
        {
        case 0:
            
            s.runStudentMenu();
             
             break;
        case 1:
        	System.out.println("\n\n|__________________________________________|");
            System.out.println("|            Online Banking                |\n");
            
            Onlinebanking ob = new Onlinebanking();
            double xob =getPamount();
            String MNob= getVerifyMatric();
            ob.runAllInMenu(xob,MNob);
           
           
            
            break;
        case 2:
        	System.out.println("\n\n|__________________________________________|");
            System.out.println("|            Credit / Debit                |\n");
            Credit cr= new Credit();
            
            double x =getPamount();
            String MN= getVerifyMatric();

            cr.performcreditpayment(x,MN);
           
           
            
            break;
        default:
            System.out.println("\n\nUnknown error has occured");
        }
    }
    
 /*************************************************************************************/
    
    public boolean editRecord(String filepath,double finalAmount,String pass,String pm/*,long NACN*/ )
    {
    	/*if(pm.equals("Online-Banking"))
    	{
    		setcardNo(0);
    		setNoAccount(NACN);
    	}else if(pm.equals("Credit/Debit"))
    	{
    		setcardNo(NACN);
    		setNoAccount(0);
    	}else
    	{
    		System.out.println("Error happened. Please run the code again");
    	}*/
    	
    	setPaymentMethod(pm);
    	String tempFile ="tempt.txt";
    	//File oldFile = new File (filepath);
    	//File newFile = new File(tempFile);
    	String name ="";String matric ="";
    	String block="";String room="";
    	double debt=0.0;String statuspayment="";
    	//long cardNo=0;long noAcc=0;
    	
    	try
    	{
    		FileWriter rd = new FileWriter(tempFile,false);
    		BufferedWriter bw = new BufferedWriter(rd);
    		PrintWriter pw = new PrintWriter(bw);
    		x = new Scanner(new File(filepath));
    		x.useDelimiter("[,\n]");
    		
    		while(x.hasNext())
    		{
    			name = x.next();
    			matric = x.next();
    			block = x.next();
    			room = x.next();
    			debt = x.nextDouble();
    			statuspayment=x.next();
    			//cardNo=x.nextLong();
    			//noAcc=x.nextLong();
    			
    			
    			
    			if(matric.equals(pass))
    			{
    				
    				pw.print(name+","+matric+","+block+","+room
    				+","+getFinalAmount()+","+getPaymentMethod()+
    				/*getcardNo()+getNoAccount()+*/"\n");
    			}
    			else
    			{
    				pw.print(name+","+matric+","+block+","+room+","+debt+","+statuspayment/*+cardNo+noAcc*/+"\n");
    			}
    		
    			
    			
    		}
    		
    		
    		x.close();
    		pw.flush();
    		pw.close();
    		Scanner y = new Scanner(new File(tempFile));
    		FileWriter fw = new FileWriter(filepath,false);
    		BufferedWriter bb = new BufferedWriter(fw);
    		PrintWriter pp = new PrintWriter(bb);
    		while(y.hasNext()) {
    			pp.println(y.next());
    		}
    		y.close();
    		pp.flush();
    		pp.close();
    		
    		
    		
    		
    		
    	}
    	catch(Exception e)
    	{
    		System.out.println("Error");
    	}
		return true;
    }
    

    
  
    
   
}
