package report;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import staff.*;
import block.*;



public class Report
{
    private Date ReportDate;
    private String ReportType;
    
    Staff s = new Staff();
    Block b = new Block();
    
    public Report() {}
    
    public Report(String ReportType, String block,String room)
    {
        ReportDate = new Date();
        this.ReportType = ReportType;
        b.setBlockNumber(block);
        b.setRoomNumber(room);
    }
    
    
    
	public void viewReport()
    {
		try
        {
                Scanner inFile = new Scanner(new File("studentReport.txt"));
                System.out.println("\n\nList of students report\n");
                String[] head = {"Date", "Type", "Block","Room"};
                inFile.useDelimiter("[,\n]");
                
                while(inFile.hasNext())
                {
                    for(int i=0; i<4; i++)
                    {
                        System.out.println(head[i] + ": " + inFile.next());
                        
                    }
                    System.out.print("****************************************\n");
                    
                }
                
                inFile.close();
        }
        catch (FileNotFoundException e)
        {
              e.printStackTrace();
        }
        
        s.cont();
		    
    }
    
    
    public void saveReport() 
    {
    	SimpleDateFormat dateForm = new SimpleDateFormat("MM/dd/Y-hh:mma");
    	String fileName = "studentReport.txt";
        try 
        {
        	FileWriter fw = new FileWriter(fileName,true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);
            
            pw.println(dateForm.format(ReportDate) + "," + ReportType + ","+b.getBlockNumber()+ ","+b.getRoomNumber());
            pw.flush();
			pw.close();
        }
        catch(Exception e) 
        {
            System.out.println("Error");
        }
        
    }
    

}
