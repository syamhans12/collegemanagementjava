package onlinebanking;
import java.util.Scanner;
import payment.*;
import student.*;


public class Onlinebanking extends Payment 
{
    
    Scanner sc = new Scanner (System.in);
    Payment p = new Payment();
    Student s = new Student();
    String filepath="C:\\Users\\hanis\\Codes\\CollegeManagementSystem\\student.txt";
    
    public void runAllInMenu(double xob,String MNob)
    {
    	double cd=xob;
    	String mNocd=MNob;
    	runObMenu();
        int c=getObChoice();
        performObpayment(c,cd,mNocd);
    }

    
    public void runObMenu()
    {
    	System.out.println("\n|                                          |");
        System.out.println("|       Choose a bank to do payment        |");
        System.out.println("|            1) CIMB-Click                 |");
        System.out.println("|            2) Maybank 2U                 |");
        System.out.println("|            3) Affin Bank                 |");
        System.out.println("|            4) Bank Islam Online          |");
        System.out.println("|            0) Exit                       |");
        System.out.println("|                                          |");
    }
    
    public int getObChoice()
    {
        Scanner kb = new Scanner (System.in);
        int choice = -1;
        while (choice <0 || choice > 4)
        {
            try {
                System.out.print("\nEnter your selection : ");
                choice=Integer.parseInt(kb.nextLine());
            }
            catch (NumberFormatException e) {
                System.out.println("Invalid selection.Please try again");
            }
            
        }
        
        return choice;
    }
    
    
    
    public void performObpayment(int choice,double xob,String MNob)
    {
    	double cd=xob;
    	String mNocd=MNob;
        switch (choice)  
        {
        case 0:
        	
        	 s.runStudentMenu();
             break;
        case 1:
            System.out.println("\n\n|__________________________________________|");
            System.out.println("|            CIMB-Click                |\n");
            paymentinput(cd,mNocd);
            
            
            break;
        case 2:
            System.out.println("\n\n|__________________________________________|");
            System.out.println("|            Maybank2U                |\n");
            paymentinput(cd,mNocd);
           
            break;
        case 3:
            System.out.println("\n\n|__________________________________________|");
            System.out.println("|             Affin Bank            |\n");
            paymentinput(cd,mNocd);
           
            break;
        case 4:
            System.out.println("\n\n|__________________________________________|");
            System.out.println("|        Bank Islam                   |\n");
            paymentinput(cd,mNocd);
           
            break;
        default:
            System.out.println("\n\nUnknown error has occured");
        }
    }
    
    
    
    public void paymentinput(double xob,String MNob)
    {
    
    	System.out.print("Enter amount you want to pay: RM ");
        double n = sc.nextDouble();
        p.setPayfee(n);
        
        System.out.print("Enter your account number: ");
        long m = sc.nextLong();
        p.setNoAccount(m);
        
        System.out.println("Your current debt :RM"+xob);
        System.out.println("The amount you pay :RM"+ p.getPayfee());
        
        double latestAmount=xob-p.getPayfee();
        if(latestAmount<=0.0)
        {
        	p.setFinalAmount(0.0);
        }
        else {
        p.setFinalAmount(latestAmount);}
        
        
        System.out.println("Your current debt :RM"+p.getFinalAmount());
        
        String pass=MNob;
        double fA =p.getFinalAmount();
        p.editRecord(filepath,fA,pass,"Online-Banking"/*,p.getNoAccount()*/);
        
        if(p.editRecord(filepath,fA,pass,"Online-Banking"/*,p.getNoAccount()*/))
        {
        	System.out.println("\n\nYour payment is succesful\n\n");
        	s.runMenu();
        	
        }else {
        	 System.out.println("Something wrong with the system. Please run again");
        }
    }
}
